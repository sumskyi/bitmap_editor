# frozen_string_literal: true

require './lib/table'

# creates and draws tables
# from given command files
module BitmapEditor
  InputError = Class.new(StandardError)
  ProcessingError = Class.new(StandardError)

  attr_reader :table

  MAX_TABLE_SIZE = 250

  class << self
    def run(file)
      @table = Table.new(0, 0)
      content = read_content(file)
      validate(content)
      process(content)
    rescue InputError, ProcessingError => error
      puts error.message
      error.message
    end

    private

    def process(content)
      _, tx, ty = content[0].split(/\s+/).map(&:to_i)

      content.each do |line|
        command = line.strip[0]

        klass = mapping[command]
        raise InputError, 'unrecognised command :(' unless klass

        klass.new(line, [tx, ty], @table).run
      end

      result = @table.render if rendered?(content)
      puts result
      result
    end

    def read_content(file)
      puts 'please provide correct file' if file.nil? || !File.exist?(file)

      File.readlines(file).reject do |line|
        line.strip =~ /^\s*$/
      end
    end

    def mapping
      @_mapping ||= {
        'I' => Create,
        'C' => Clear,
        'L' => Pixel,
        'V' => Column,
        'H' => Row,
        'S' => Render
      }
    end

    def validate(content)
      unless created?(content)
        raise InputError, "missing 'I' command at the beginning of file"
      end

      unless fit_size?(content)
        raise InputError, 'table dimensions should be from 1x1 to 250x250'
      end

      unless rendered?(content)
        raise InputError, "missing 'S' command at the end of file"
      end
      true
    end

    def created?(content)
      content[0][0].strip == 'I'
    end

    def fit_size?(content)
      _, x, y = content[0].split(/\s+/).map(&:to_i)
      allowed_range = (1..MAX_TABLE_SIZE)
      allowed_range.cover?(x) && allowed_range.cover?(y)
    end

    def rendered?(content)
      content[-1][0].strip == 'S'
    end
  end

  # base class
  class Command
    def initialize(line, table_data, table)
      @line = line
      @tx, @ty = table_data
      @table = table
    end

    def run
      validate
      process
    end

    def validate
      raise NotImplementedError
    end

    def process
      raise NotImplementedError
    end

    def number?(str)
      str =~ /\A\d+\z/
    end
  end

  # table creation
  class Create < Command
    private

    def validate
      true
    end

    def process
      _, x, y = @line.split(/\s+/)
      @table.resize(x.to_i, y.to_i)
    end
  end

  # draw 1 pixel
  class Pixel < Command
    def validate
      _, x, y, = @line.split(/\s+/)

      raise InputError, 'pixel is not a number' unless numbers?([x, y])
      raise InputError, 'pixel is outside of a table' unless valid_coords?(x, y)
    end

    def process
      _, x, y, value = @line.split(/\s+/)
      @table.set(x: x.to_i, y: y.to_i, value: value)
    end

    def valid_coords?(x, y)
      x.to_i <= @tx && y.to_i <= @ty
    end

    def numbers?(point)
      point.all? { |el| number?(el) }
    end
  end

  # draw line
  class Row < Command
    def validate
      _, from_column, to_column, row, = @line.split(/\s+/)

      unless valid_dimensions?(from_column, to_column)
        raise InputError, 'column is out of range'
      end

      unless numbers?([from_column, to_column, row])
        raise InputError, 'from column, to column and row should be numbers'
      end
      true
    end

    def process
      _, y1, y2, x, value = @line.split(/\s+/)
      @table.horizontal(column1: y1.to_i,
                        column2: y2.to_i,
                        row: x.to_i,
                        value: value)
    end

    def valid_dimensions?(from_column, to_column)
      from_column.to_i < to_column.to_i && \
        from_column.to_i >= 1 && from_column.to_i <= @tx && \
        to_column.to_i <= @tx
    end

    def numbers?(coords)
      coords.all? { |el| number?(el) }
    end
  end

  # draw column
  class Column < Command
    def validate
      _, column, from_row, to_row, = @line.split(/\s+/)
      unless valid_dimensions?(from_row, to_row)
        raise InputError, 'row is out of range'
      end

      unless numbers?([from_row, to_row, column])
        raise InputError, 'from row, to row and column should be numbers'
      end
      true
    end

    def process
      _, y, x1, x2, value = @line.split(/\s+/)
      @table.vertical(row1: x1.to_i,
                      row2: x2.to_i,
                      column: y.to_i,
                      value: value)
    end

    def valid_dimensions?(from_row, to_row)
      from_row.to_i < to_row.to_i && \
        from_row.to_i >= 1 && from_row.to_i <= @ty && \
        to_row.to_i <= @ty
    end

    def numbers?(coords)
      coords.all? { |el| number?(el) }
    end
  end

  # stub class
  class Render < Command
    def validate; end

    def process; end
  end

  # make the table great again
  class Clear < Command
    def run
      @table.clear
    end
  end
end
