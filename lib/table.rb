# frozen_string_literal: true

require 'forwardable'

# create and manipulate by the table
class Table
  extend Forwardable

  attr_reader :rows, :columns
  def_delegators :@table, :[]

  OutOfTable = Class.new(StandardError)

  def initialize(rows, columns)
    @rows = rows
    @columns = columns
    clear
  end

  def render
    @table.map { |row| row.join ' ' }.join("\n")
  end

  def set(x:, y:, value:)
    raise OutOfTable, 'wrong cell' if x > @columns || y > @rows

    @table[y - 1][x - 1] = value
  end

  def vertical(row1:, row2:, column:, value:)
    (row1..row2).each do |row|
      set(y: row, x: column, value: value)
    end
  end

  def horizontal(column1:, column2:, row:, value:)
    (column1..column2).each do |column|
      set(y: row, x: column, value: value)
    end
  end

  def get(x:, y:)
    raise OutOfTable, 'wrong cell' if x > @columns || y > @rows

    @table[y - 1][x - 1]
  end

  def clear
    @table = Array.new(@rows) { Array.new(@columns, 0) }
  end

  def resize(x, y)
    @rows = y
    @columns = x
    @table = Array.new(y) { Array.new(x, 0) }
  end
end
