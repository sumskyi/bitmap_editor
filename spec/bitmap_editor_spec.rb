# frozen_string_literal: true

require './lib/bitmap_editor'

describe BitmapEditor do
  subject { BitmapEditor.run(filename) }

  let(:filename) { './spec/fixtures/valid.cmd' }

  let(:ok) do
'0 0 0 0 0
0 0 Z Z Z
A W 0 0 0
0 W 0 0 0
0 W 0 0 0
0 W 0 0 0'
  end

  it 'works' do
    expect(subject).to eq ok
  end
end
