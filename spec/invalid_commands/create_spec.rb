# frozen_string_literal: true

require 'spec_helper'
require './lib/bitmap_editor'

describe 'Invalid Commands' do
  subject { BitmapEditor.run(filename) }

  let(:filename) { "./spec/fixtures/invalid_commands/table/#{command}.cmd" }

  describe 'file contains empty rows' do
    let(:command) { 'empty_rows' }

    it 'rejects empty lines' do
      expect(subject).not_to be_nil
    end
  end

  describe 'table was not created' do
    let(:command) { 'not_created' }

    it 'validates if table was created' do
      expect(subject).to eq("missing 'I' command at the beginning of file")
    end
  end

  describe 'out of range' do
    let(:command) { 'out_of_range' }

    it 'validates if table dimensions are in 1..250' do
      expect(subject).to eq('table dimensions should be from 1x1 to 250x250')
    end
  end
end
