# frozen_string_literal: true

require 'spec_helper'
require './lib/bitmap_editor'

describe 'Pixel Invalid Commands' do
  subject { BitmapEditor.run(filename) }

  let(:filename) { "./spec/fixtures/invalid_commands/pixel/#{command}.cmd" }

  describe 'out of range' do
    let(:command) { 'out_of_range' }

    it 'returns error on bad coords' do
      expect(subject).to eq 'pixel is outside of a table'
    end
  end

  describe 'not a number' do
    let(:command) { 'not_a_number' }

    it 'returns error on bad numbers' do
      expect(subject).to eq 'pixel is not a number'
    end
  end
end
