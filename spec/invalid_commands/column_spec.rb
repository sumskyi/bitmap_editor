# frozen_string_literal: true

require 'spec_helper'
require './lib/bitmap_editor'

describe 'Column Invalid Commands' do
  subject { BitmapEditor.run(filename) }

  let(:filename) { "./spec/fixtures/invalid_commands/column/#{command}.cmd" }

  describe 'out of range' do
    let(:command) { 'out_of_range' }

    it 'returns error on bad coords' do
      expect(subject).to eq 'row is out of range'
    end
  end

  describe 'not a number' do
    let(:command) { 'not_a_number' }

    it 'returns error when provided not numers' do
      expect(subject).to eq 'from row, to row and column should be numbers'
    end
  end
end
