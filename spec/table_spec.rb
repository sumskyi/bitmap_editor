# frozen_string_literal: true

require 'spec_helper'
require './lib/table'

describe Table do
  let(:rows) { 10 }
  let(:columns) { 20 }

  subject(:table) { Table.new(rows, columns) }

  it '#rows' do
    expect(table.rows).to eq rows
  end

  it '#columns' do
    expect(table.columns).to eq columns
  end

  it '#render' do
    expect(table.render).to be_an_instance_of(String)
  end

  describe '#set' do
    it 'raises exception on wrong cell' do
      expect { table.set(x: 100, y: 200, value: 345) }.to \
        raise_error(Table::OutOfTable)
    end

    it 'set value' do
      table.set(x: 20, y: 10, value: 'Z')

      expect(table[9][19]).to eq 'Z'
    end
  end

  it '#get' do
    table.set(x: 2, y: 3, value: 'Z')

    expect(table.get(x: 2, y: 3)).to eq 'Z'
  end

  it '#clear' do
    table.set(x: 2, y: 3, value: 'Z')
    table.clear
    expect(table.get(x: 2, y: 3)).to eq 0
  end

  it '#vertical' do
    table.vertical(row1: 2, row2: 4, column: 3, value: 'Q')

    expect(table.get(y: 2, x: 3)).to eq 'Q'
    expect(table.get(y: 3, x: 3)).to eq 'Q'
    expect(table.get(y: 4, x: 3)).to eq 'Q'
  end

  it '#horizontal' do
    table.horizontal(column1: 2, column2: 4, row: 3, value: 'Z')

    expect(table.get(x: 2, y: 3)).to eq 'Z'
    expect(table.get(x: 3, y: 3)).to eq 'Z'
    expect(table.get(x: 4, y: 3)).to eq 'Z'
  end
end
